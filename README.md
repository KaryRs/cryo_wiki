# Wiki de Cryo

## Objetivo del repositorio
Este espacio tiene como fin centralizar toda la documentación relacionada con los procesos definidos por el departamento. Se espera que sea utilizado para consultar en cualquier momento para recordar los pasos, actividades o productos de trabajo que se requieren al estar implementado cualquiera de los procesos del departamento.

Este espacio tiene como fin centralizar toda la documentación relacionada con los procesos definidos por el departamento. Se espera que sea utilizado para consultar en cualquier momento para recordar los pasos, actividades o productos de trabajo que se requieren al estar implementado cualquiera de los procesos del departamento.

**Los archivos presentes en este repositorio se consideran las _últimas versiones_, revisadas y aprobadas por todo el departamento.**

* * * *

[comment]: <> (Inicio de Contenido)


## Información General de Cryo

1.[Departamento Cryo](#markdown-header-departamento-cryo)

* * * *

## Carpeta Estática

2.[Carpeta Estática](#markdown-header-estatica)

2.1 [Documento 1](#markdown-header-doc1)

2.2 [Documento 2](#markdown-header-doc2)

2.3 [Documento 3](#markdown-header-doc3)

2.4 [Documento 4](#markdown-header-doc4)

* * * *

## Carpeta Controlada

3.[Requisitos funcionales](link)

3.1 [Criterios de aceptación](link)

3.2 [Requisitos no funcionales](link)

3.3 [MER](link)

3.4 [Stack tecnología](link)

3.5 Acta constitutiva

3.6 Proceso Cryo

3.7 Estándar de código

3.8 Estándar de diseño

3.9 Estándar de documentación

[comment]: <> (Faltan los despliegues de cada requisito)

3.10 


* * * *

## Carpeta Dinámica

4.[Carpeta Controlada](#markdown-header-dinamica)

4.1 [Documento 111](#markdown-header-doc111)

4.2 [Documento 222](#markdown-header-doc222)

4.3 [Documento 333](#markdown-header-doc333)

4.4 [Documento 444](#markdown-header-doc444)

* * * *

## No controlado con versiones

5.[PSP](https://docs.google.com/spreadsheets/d/1sZPrpT8TZ3UKyc20C5hJ0kJ_JE895WJGtT0BQoo8GeM/edit#gid=0)

5.1 [Burndown/valor ganado](https://docs.google.com/spreadsheets/d/14IompTAECBXj9raD8338l3oLydYqggj2kDxO3IxTvGo/edit#gid=2052696054)

5.2 [Riesgos](https://docs.google.com/spreadsheets/d/1aHk6JVHQfLcbZ8DPlsE8_Np30Gy9JEKZuD-QfEdASwI/edit#gid=1339433906)

5.3 [Cambios](https://docs.google.com/spreadsheets/d/12zmHHPumMqnY40efjugVa31qiubC-Kb6mtTPlNLzVoo/edit#gid=1683981855)

   